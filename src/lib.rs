extern crate serde;
#[macro_use]
extern crate serde_derive;
extern crate rmp_serde as rmps;
extern crate rmp;

pub mod connection;

pub use connection::*;

#[derive(Copy, Clone, Debug, Serialize, Deserialize)]
pub struct SpawnPart {
    pub obj_type: u8,
    pub mirror: bool,
    pub pos: [u32; 2],
    pub ang: u8
}

#[derive(Copy, Clone, Debug, Serialize, Deserialize)]
pub enum GridMsg {
    AddPart(SpawnPart),
    RemovePart(u32),
    ChangePart(u32, SpawnPart),
    Start
}

#[derive(Copy, Clone, Debug, Serialize, Deserialize)]
pub struct GameObject {
    pub obj_type: u8,
    pub mirror: bool,
    pub size: [f32; 2],
    pub pos: [f32; 2],
    pub vel: [f32; 2],
    pub ang: f32,
    pub ang_vel: f32
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub enum GameMsg {
    Sensor(f32, f32, Vec<GameObject>),
    Connector(u32, u32)
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub enum Msg {
    SpawnStage([u32; 2]),
    GameStage,
    Spawn(GridMsg),
    Game(GameMsg)
}
